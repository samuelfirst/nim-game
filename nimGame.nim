#[
nimGame
Copyright (C) 2018 Samuel First

    nimGame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    nimGame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with nimGame.  If not, see <https://www.gnu.org/licenses/>.
]#

import osproc
import random
import math
from strutils import parseInt

# For each pile:
#  print an 'O' for each item in the pile
proc displayBoard(piles: seq[int]): void =
  ## Clear screen and print board
  
  discard execCmd "clear"
  
  for index, pile in piles:
    stdout.write index, ": "
    for i in 1..pile:
      stdout.write "O"
    stdout.write "\n"


# Prompt whether or not the player wants
# a single, or multi-player game; return bool
proc isOnePlayer(): bool =
  ## Ask if game is one or two players; default to one
  
  stdout.write "How Many Players? [ONE/two] "
  case readLine(stdin)
  of "Two", "two", "2":
    return false
  else:
    return true


# Return should be [number of piles,
#                   max size of pile,
#                   min size of pile]
proc promptForSpecification(): seq[int] =
  ## Get specifications of how game should be set up
  while true:
    try:
      stdout.write "How many piles? "
      var pileNum: int = parseInt(readLine(stdin))
      stdout.write "Max pile size? "
      var maxSize: int = parseInt(readLine(stdin))
      stdout.write "Min pile size? "
      var minSize: int = parseInt(readLine(stdin))
      
      return @[pileNum, maxSize, minSize]
    except ValueError:
      echo "\nIntegers only please.\n"


proc promptForExit(): bool =
  ## Prompt player to exit; default to yes
  
  stdout.write "Exit [Y/n] "
  case readLine(stdin)
  of "N", "n", "No", "no":
    return false
  else:
    return true


# Get human player move and adjust piles accordingly
proc playerTurn(piles: var seq[int], player: string): void =
  ## Prompt for move from human player
  while true:
    try:
      echo "Player: ", player
      stdout.write "Which pile do you want to modify? "
      var pile: int = parseInt(readLine(stdin))
      
      stdout.write "How many stones do you want to take? "
      var amount: int = parseInt(readLine(stdin))

      piles[pile] -= amount
      break
    except ValueError:
      echo "\nIntegers only please.\n"


# Convert int to binary for calculating AI's move
proc toBin(number: int): string =
  ## Convert integer to binary

  var num: int = number
  var bin: string = ""
  var revBin: string = ""

  # Calculate bits
  while num / 2 > 0:
    bin.add($(num mod 2))
    num = num div 2

  # Pad out to one byte
  for i in 1..8-(bin.high+1):
    bin.add("0")

  # Reverse
  for i in countDown(bin.high, 0):
    revBin.add(bin[i])
    
  return revBin


# Convert binary to int for calculating AI's move
proc fromBin(binary: string): int =
  ## Convert binary to integer

  var num: int = 0
  var exp: int = 2^(len(binary)-1)
  for bit in binary:
    num += parseInt($(bit))*exp
    exp = exp div 2
  return num


# Applies xor to bytes, returns resulting byte,
# for AI move calculation
proc binXor(bytes: seq[string]): string =
  ## Apply xor to sequence of bytes
  for bit in 0..7:
    var index: int = 0
    var curBit: int = parseInt($(bytes[index][bit]))
    while index < len(bytes) - 1:
      curBit = curBit xor parseInt($(bytes[index+1][bit]))
      index += 1
      
    result.add(curBit)


# Make move for computer:
#  * Calculate Nim Sum:
#   * Convert piles to binary
#   * Xor the binary piles
#  * Xor the Nim Sum with each pile
#  * If a pile is less than the Nim Sum'd pile:
#   * Set pile equal to the Nim Sum'd pile
#  * Repeat until win
proc aiTurn(piles: var seq[int]): void =
  ## Makes move for machine in single player mode
  echo "Computer Player Thinking..."
  
  # Get Nim Sum
  var binPiles: seq[string]
  for pile in piles:
    binPiles.add(toBin(pile))
  var nimSum: string = binXor(binPiles)

  # Check each pile against nim sum
  for i in 0..<len(piles):
    var checkedPile = fromBin(binXor(@[nimSum, toBin(piles[i])]))
    if piles[i] > checkedPile:
      piles[i] = checkedPile
      break


# Check if all piles are <= 0
proc checkForWin(piles: seq[int]): bool =
  ## Checks to see if the game has been won

  for pile in piles:
    if pile > 0:
      return false
  displayBoard(piles)  # Displays empty board if player has won
  return true


proc onePlayer(piles: var seq[int]): bool =
  ## Print board, make AI move, and request move from player

  displayBoard(piles)
  playerTurn(piles, "Human Player")
  if checkForWin(piles):
    echo "Human Player Wins!"
    return false

  displayBoard(piles)
  aiTurn(piles)
  if checkForWin(piles):
    echo "Computer Player Wins!"
    return false

  return true


proc twoPlayer(piles: var seq[int]): bool =
  ## Print board and request moves from both players

  for player in ["Player One", "Player Two"]:
    displayBoard(piles)
    playerTurn(piles, player)
    if checkForWin(piles):
      echo player, " Wins!"
      return false
  return true
  
    
# Loop until user requests exit
while true:
  var stack: seq[int]
  var spec: seq[int] = promptForSpecification()
  randomize()
  for i in 1..spec[0]:
    stack.add(random(spec[2]..spec[1]+1))
  
  if isOnePlayer():
    while onePlayer(stack):
      continue
  else:
    while twoPlayer(stack):
      continue
  
  if promptForExit():
    break
