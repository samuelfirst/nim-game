# Nim Game

The game of nim, written in nim.

The game supports two game modes: single player and multi-player

## Building
* Step 1: Install Nim
  * You can find installation instructions here: https://nim-lang.org/install.html
    * Note: If you are building this on windows, change 'clear' on line 29 to 'cls'

* Step 2: Compile the script
  * In the project directory, run `nim c nimGame.nim`

## Rules
The game begins with a set of stacks, each containing a random number of items.

Per turn, each player may remove any number of items from a single stack

The player who is last to remove an item wins.


### Example:

#### Start:

0: OOOOOOOOO

1: OOOOOO

2: OOOOOOOOOO


#### Turn 1:

Player One removes 3 items from stack 0

0: OOOOOO

1: OOOOOO

2: OOOOOOOOOO

Player Two removes 10 items from stack 2

0: OOOOOO

1: OOOOOO

2:


#### Turn 2:

Player One removes 4 items from stack 1

0: OOOOOO

1: OO

2:

Player Two removes 4 items from stack 0

0: OO

1: OO

2:


#### Turn 3:

Player One removes 2 items from stack 0

0:

1: OO

2:

Player Two removes 2 items from stack 1

Player Two Wins

## License
nimGame is distrubuted under the GPLv3.  This means you are free to modify and/or redistribute it.  See COPYING for more details.
